; get the list pointer
;mov eax, [esp]

;looper:
;cmp dword [eax+4],41414100h
;je short matched
;increment
;mov eax,[eax]
;jmp short looper
;matched:
; get the data pointer
;add al,8


pop ecx
_loop:
pop esi
lodsd
cmp word ptr[ecx+5],4141h
push eax
jne _loop
add al,8
jmp ecx

; When assembled becomes:
;    Hex dump                   Command
;      8B04E4                   MOV EAX,DWORD PTR SS:[ESP]
;  looper :
;      8178 04 00414141         CMP DWORD PTR DS:[EAX+4],41414100
;      74 04                    JE SHORT matched
;      8B00                     MOV EAX,DWORD PTR DS:[EAX]
;      EB F0                    JMP SHORT looper
;  matched:
;      04 08                    ADD AL,8
;
; Total 18 bytes
;

; possible reusage of 41414100h
; 0041 41         ADD BYTE PTR DS:[ECX+41],AL
; 41              INC ECX
